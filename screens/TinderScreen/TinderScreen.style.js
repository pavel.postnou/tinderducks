import { StyleSheet } from "react-native";

import { width, height } from "../../constants/sizeConstants";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  picturesContainer: { flex: 1 },
  textContainer: {
    position: "absolute",
    bottom: 25,
    left: 30,
    zIndex: 1000,
  },
  forwardButton: {
    position: "absolute",
    right: 0,
    height: height - 200,
    width: 50,
  },
  backButton: {
    position: "absolute",
    left: 0,
    height: height - 200,
    width: 50,
  },
  buttonContainer: {
    width: width,
    flexDirection: "row",
    justifyContent: "space-evenly",
    position: "absolute",
    bottom: 0,
    marginBottom: 50,
  },
});
