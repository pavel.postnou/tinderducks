import React, { useState, useEffect, useMemo } from "react";
import {
  View,
  Animated,
  PanResponder,
  TouchableWithoutFeedback,
} from "react-native";
import { Icon } from "react-native-elements";
import {
  collection,
  getDocs,
  getDoc,
  doc,
  updateDoc,
} from "firebase/firestore";

import TextComponent from "../../src/shared/components/TextComponent";
import ImageComponent from "../../src/shared/components/ImageComponent";
import { db } from "../../firebase";
import { width, height } from "../../constants/sizeConstants";

import { styles } from "./TinderScreen.style";

const TinderScreen = (props) => {
  const email = props.route.params.email;
  const [photoIndex, setPhotoIndex] = useState(0);
  const [currentIndex, setCurrentIndex] = useState(0);
  const pan = new Animated.ValueXY();
  const [users, setUsers] = useState([]);

  useEffect(async () => {
    const arr = [];
    const querySnapshot = await getDocs(collection(db, "users"));
    querySnapshot.forEach((doc) => {
      doc.id === email
        ? null
        : arr.push({
            doc: doc.id,
            pictures: doc.data().pictures,
            about: doc.data().about,
          });
    });
    setUsers(arr);
  }, [props]);

  const like = async () => {
    let arr = [];
    const docSnap = await getDoc(doc(db, "users", users[currentIndex].doc));
    arr = docSnap.data().likes;
    arr.indexOf(email) != -1
      ? null
      : (arr.push(email),
        await updateDoc(doc(db, "users", users[currentIndex].doc), {
          likes: arr,
        }));
    setCurrentIndex(currentIndex + 1), setPhotoIndex(0);
  };

  const getBack = async () => {
    let arr = [];
    const docSnap = await getDoc(doc(db, "users", users[currentIndex - 1].doc));
    arr = docSnap.data().likes;
    arr.indexOf(email) != -1
      ? (arr.splice(arr.indexOf(email), 1),
        await updateDoc(doc(db, "users", users[currentIndex - 1].doc), {
          likes: arr,
        }))
      : null;
    setCurrentIndex(currentIndex - 1);
  };

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onPanResponderMove: Animated.event(
      [
        null,
        {
          dx: pan.x,
        },
      ],
      { useNativeDriver: false }
    ),
    onPanResponderRelease: (e, gestureState) => {
      if (gestureState.dx > 120) {
        like();
        Animated.spring(pan, {
          toValue: { x: width + 150, y: pan.y },
          useNativeDriver: false,
          friction: 4,
        }).start(
          setCurrentIndex(currentIndex + 1),
          pan.setValue({ x: 0, y: 0 }),
          setPhotoIndex(0)
        );
      } else if (gestureState.dx < -120) {
        Animated.spring(pan, {
          toValue: { x: -width - 150, y: pan.y },
          useNativeDriver: false,
          friction: 4,
        }).start(
          setCurrentIndex(currentIndex + 1),
          pan.setValue({ x: 0, y: 0 }),
          setPhotoIndex(0)
        );
      } else {
        Animated.spring(pan, {
          toValue: { x: 0, y: 0 },
          useNativeDriver: false,
          friction: 4,
        }).start();
      }
    },
  });

  // const animated = () => {
  //   Animated.spring(pan, {
  //     toValue: { x: -width - 150, y: pan.y },
  //     useNativeDriver: false,
  //     friction: 4,
  //   }).start()
  // };

  const tinderView = useMemo(() => {
    return users
      ?.map((item, i) => {
        if (i < currentIndex) {
          return null;
        } else if (i == currentIndex) {
          return (
            <Animated.View
              {...panResponder.panHandlers}
              props
              key={i}
              style={[
                {
                  transform: [
                    { translateX: pan.x },
                    { translateY: pan.y },
                    {
                      rotate: pan.x.interpolate({
                        inputRange: [-200, 0, 200],
                        outputRange: ["-20deg", "0deg", "20deg"],
                      }),
                    },
                  ],
                  height: height - 200,
                  width: width,
                  padding: 20,
                  marginTop: 15,
                  position: "absolute",
                },
              ]}
            >
              <Animated.View
                style={{
                  transform: [{ rotate: "-30deg" }],
                  opacity: pan.x.interpolate({
                    inputRange: [-150, 0, 150],
                    outputRange: [0, 0, 1],
                    extrapolate: "clamp",
                  }),
                  position: "absolute",
                  top: 50,
                  left: 40,
                  zIndex: 1000,
                }}
              >
                <TextComponent style="like" text="LIKE" />
              </Animated.View>
              <View style={styles.textContainer}>
                <TextComponent style="tinderAbout" text={item.about} />
              </View>
              <Animated.View
                style={{
                  transform: [{ rotate: "30deg" }],
                  opacity: pan.x.interpolate({
                    inputRange: [-150, 0, 150],
                    outputRange: [1, 0, 0],
                    extrapolate: "clamp",
                  }),
                  position: "absolute",
                  top: 50,
                  right: 40,
                  zIndex: 1000,
                }}
              >
                <TextComponent style="nope" text="NOPE" />
              </Animated.View>
              <ImageComponent uri={item.pictures[photoIndex].url} />
              <TouchableWithoutFeedback
                onPress={() =>
                  photoIndex < item.pictures.length - 1
                    ? setPhotoIndex(photoIndex + 1)
                    : null
                }
              >
                <View style={styles.forwardButton}></View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() =>
                  photoIndex > 0 ? setPhotoIndex(photoIndex - 1) : null
                }
              >
                <View style={styles.backButton}></View>
              </TouchableWithoutFeedback>
            </Animated.View>
          );
        } else {
          return (
            <Animated.View
              key={i}
              style={{
                height: height - 200,
                width: width,
                padding: 10,
                position: "absolute",
                opacity: pan.x.interpolate({
                  inputRange: [-150, 0, 150],
                  outputRange: [1, 0, 1],
                  extrapolate: "clamp",
                }),
                transform: [
                  {
                    scale: pan.x.interpolate({
                      inputRange: [-250, 0, 250],
                      outputRange: [0.9, 0.8, 0.9],
                    }),
                  },
                ],
              }}
            >
              <ImageComponent uri={item.pictures[0].url} />
            </Animated.View>
          );
        }
      })
      .reverse();
  }, [users]);

  return (
    <View style={styles.container}>
      <View style={styles.picturesContainer}>
        {tinderView}
        <View style={styles.buttonContainer}>
          <Icon
            raised
            name="times"
            type="font-awesome"
            color="#f50"
            size={30}
            onPress={() => (
              setCurrentIndex(currentIndex + 1), setPhotoIndex(0)
            )}
          />
          <Icon
            raised
            name="undo"
            type="font-awesome"
            color="#fcba03"
            size={30}
            onPress={() => (currentIndex > 0 ? getBack() : null)}
          />
          <Icon
            raised
            name="heartbeat"
            type="font-awesome"
            color="green"
            size={30}
            onPress={like}
          />
        </View>
      </View>
    </View>
  );
};

export { TinderScreen };
