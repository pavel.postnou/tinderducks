import React, { useEffect, useState } from "react";
import { useNavigation } from "@react-navigation/core";
import * as WebBrowser from "expo-web-browser";
import * as Google from "expo-auth-session/providers/google";
import {
  View,
  KeyboardAvoidingView,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import {
  getAuth,
  GoogleAuthProvider,
  signInWithCredential,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from "firebase/auth";

import GoogleIcon from "../../src/shared/icons/google";

import { styles } from "./LoginScreen.style";

WebBrowser.maybeCompleteAuthSession();

function LoginScreen() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigation = useNavigation();
  const auth = getAuth();
  const backgroundImage = require("../../assets/main-back.jpg");
  const [request, response, promptAsync] = Google.useIdTokenAuthRequest({
    clientId:
      "220616473177-3o0k3oqcuc926r1ehb7enp8j1ie9uh6c.apps.googleusercontent.com",
  });

  useEffect(async () => {
    if (response?.type === "success") {
      const { id_token } = response.params;
      const credential = GoogleAuthProvider.credential(id_token);
      await signInWithCredential(auth, credential);
      auth.onAuthStateChanged((user) => {
        user
          ? ((email = auth.currentUser.email),
            navigation.replace("Main", { email: email }))
          : null;
      });
    }
  }, [response]);

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      if (user) {
        const currentUser = user.email;
        navigation.replace("Main", { email: currentUser });
      }
    });
    return unsubscribe;
  }, []);

  const handleSignUp = () => {
    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredentials) => {
        const user = userCredentials.user;
      })
      .catch((error) => alert(error.message));
  };

  const handleLogin = () => {
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredentials) => {
        const user = userCredentials.user;
      })
      .catch((error) => alert(error.message));
  };

  return (
    <KeyboardAvoidingView>
      <ImageBackground
        style={styles.imageBackground}
        source={backgroundImage}
        resizeMode="cover"
      >
        <View style={styles.container}>
          <View style={styles.inputContainer}>
            <TextInput
              placeholder="Email"
              value={email}
              onChangeText={(text) => setEmail(text)}
              style={styles.input}
            />
            <TextInput
              placeholder="Password"
              value={password}
              onChangeText={(text) => setPassword(text)}
              style={styles.input}
              secureTextEntry
            />
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={handleLogin} style={styles.button}>
              <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={handleSignUp}
              style={[styles.button, styles.buttonOutline]}
            >
              <Text style={styles.buttonOutlineText}>Register</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => promptAsync()}>
              <GoogleIcon />
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}

export { LoginScreen };
