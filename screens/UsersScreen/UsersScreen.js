import React, { useRef, useState, useEffect } from "react";
import { View, ScrollView, TextInput } from "react-native";
import { Icon } from "react-native-elements";
import {
  addDoc,
  doc,
  updateDoc,
  getDoc,
  onSnapshot,
  collection,
  arrayUnion,
} from "firebase/firestore/";

import { db } from "../../firebase";
import TouchableComponent from "../../src/shared/components/TouchableComponent";
import ChatComponent from "../../src/shared/components/ChatComponent";

import { styles } from "./UsersScreen.style";

const UsersScreen = (props) => {
  const email = props.route.params.email;
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState({});
  const [text, setText] = useState("");
  const [messages, setMessages] = useState([]);
  const [friendChat, setFriendChat] = useState(null);
  const scrollViewRef = useRef();

  useEffect(async () => {
    const arr = [];
    let usersArr = [];
    const likes = await getDoc(doc(db, "users", email));
    if (likes.data()) {
      setUser(likes.data());
      for (let i = 0; i < likes.data().likes.length; i++) {
        arr.push(likes.data().likes[i]);
      }
    }
    for (let i = 0; i < arr.length; i++) {
      const userDoc = await getDoc(doc(db, "users", arr[i]));
      usersArr.push({
        email: arr[i],
        picture: userDoc.data().pictures[0].url,
      });
    }
    setUsers(usersArr);
  }, [friendChat, messages]);

  useEffect(() => {
    friendChat
      ? onSnapshot(doc(db, "chats", friendChat.chatId), (doc) => {
          setMessages(doc.data().messages);
          scrollViewRef.current?.scrollToEnd({ animated: true });
        })
      : null;
  }, [friendChat]);

  const chatWith = async (item) => {
    let chatDoc;
    let newChat;
    let chat = user.chats.find((chat) => chat.friendEmail === item.email);
    chat
      ? (setMessages(chat), setFriendChat(chat))
      : (setFriendChat(item.email),
        (chatDoc = await addDoc(collection(db, "chats"), {
          messages: [],
        })),
        (newChat = await updateDoc(doc(db, "users", email), {
          chats: arrayUnion({ friendEmail: item.email, chatId: chatDoc.id }),
        })),
        await updateDoc(doc(db, "users", item.email), {
          chats: arrayUnion({ friendEmail: email, chatId: chatDoc.id }),
        }),
        setFriendChat(newChat));
    scrollViewRef.current?.scrollToEnd({ animated: true });
  };

  const sendMessage = async () => {
    let arr = [];
    const likes = await getDoc(doc(db, "chats", friendChat.chatId));
    arr = likes.data().messages;
    arr.push({ author: email, message: text });
    await updateDoc(doc(db, "chats", friendChat.chatId), {
      messages: arr,
    });
    setText("");
  };

  return (
    <View style={styles.container}>
      <ScrollView horizontal style={styles.scrollContainer}>
        {users.length > 0
          ? users.map((item, index) => (
              <TouchableComponent
                key={index}
                onPress={() => chatWith(item)}
                item={item}
              />
            ))
          : null}
      </ScrollView>
      <ScrollView style={styles.scrollChatContainer} ref={scrollViewRef}>
        {messages.length > 0
          ? messages.map((item, index) => (
              <ChatComponent key={index} email={email} item={item} />
            ))
          : null}
      </ScrollView>
      <View style={styles.textInputContainer}>
        <TextInput
          style={styles.textInput}
          onChangeText={(text) => setText(text)}
          value={text}
          placeholder="message"
        />
        <Icon
          name="commenting"
          type="font-awesome"
          color="#f50"
          onPress={sendMessage}
        />
      </View>
    </View>
  );
};

export { UsersScreen };
