import { StyleSheet } from "react-native";

import { height, width } from "../../constants/sizeConstants";

export const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    marginTop: 20,
  },
  scrollContainer: {
    marginHorizontal: 10,
    height: height / 4,
  },
  scrollChatContainer: {
    height: height / 1.2,
    width: width - 20,
    backgroundColor: "lightgrey",
    marginBottom: 10,
    borderRadius: 10,
    alignSelf: "center",
    marginBottom: 10,
  },
  textInputContainer: {
    flexDirection: "row",
    width: width,
    marginBottom: 80,
  },
  textInput: {
    height: 30,
    width: width - 60,
    borderWidth: 1,
    padding: 5,
    borderRadius: 10,
    marginHorizontal: 10,
  },
});
