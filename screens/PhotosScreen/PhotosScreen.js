import React, { useState, useEffect } from "react";
import { View } from "react-native";
import SortableGrid from "react-native-sortable-grid";
import { doc, updateDoc, getDoc } from "firebase/firestore/";

import ButtonComponent from "../../src/shared/components/ButtonComponent";
import { db } from "../../firebase";
import ImageComponent from "../../src/shared/components/ImageComponent";

const PhotosScreen = (props) => {
  const [images, setImages] = useState([]);
  const email = props.route.params.email;
  useEffect(async () => {
    const docSnap = await getDoc(doc(db, "users", email));
    if (docSnap.data()) {
      const arr = [];
      for (let i = 0; i < docSnap.data().pictures.length; i++) {
        arr.push(docSnap.data().pictures[i]);
      }
      setImages(arr);
    }
  }, []);

  const updatePhotosPosition = ({ itemOrder }) => {
    const arr = [];
    for (let i = 0; i < itemOrder.length; i++) {
      arr.splice(i, 0, images[parseInt(itemOrder[i].key)]);
    }
    setImages(arr);
  };

  const savePhotosPosition = async () => {
    await updateDoc(doc(db, "users", email), {
      pictures: images,
    });
  };

  return (
    <View>
      <SortableGrid
        blockTransitionDuration={400}
        activeBlockCenteringDuration={200}
        itemsPerRow={3}
        dragActivationTreshold={100}
        onDragRelease={(itemOrder) => updatePhotosPosition(itemOrder)}
      >
        {images.map((item, index) => (
          <ImageComponent key={index} uri={item.url} />
        ))}
      </SortableGrid>
      <ButtonComponent
        title="Save"
        iconName="floppy-o"
        buttonStyle="buttonStyle"
        containerStyle="containerStyle"
        onPress={savePhotosPosition}
      />
    </View>
  );
};

export { PhotosScreen };
