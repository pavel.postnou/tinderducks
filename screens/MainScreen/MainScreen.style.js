import { StyleSheet } from "react-native";

import { height, width } from "../../constants/sizeConstants";

export const styles = StyleSheet.create({
  container: {
    width: width,
    height: height - 50,
    alignItems: "center",
    backgroundColor: "white",
  },
  pictureContainer: {
    width: width - 50,
    height: height - 200,
    alignItems: "center",
    marginTop: 20,
  },
  textInputContainer: {
    flexDirection: "row",
    marginTop: 30,
  },
  textInput: {
    height: 30,
    width: width - 60,
    borderWidth: 1,
    padding: 5,
    borderRadius: 10,
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: "row",
  },
});
