import React, { useState, useEffect, useMemo } from "react";
import { View, TextInput } from "react-native";
import { Icon } from "react-native-elements";
import { setDoc, doc, updateDoc, getDoc } from "firebase/firestore/";
import { getAuth } from "firebase/auth";
import { useNavigation } from "@react-navigation/core";

import ButtonComponent from "../../src/shared/components/ButtonComponent";
import UploadModalComponent from "../../src/shared/components/UploadModalComponent";
import ImageComponent from "../../src/shared/components/ImageComponent";
import TextComponent from "../../src/shared/components/TextComponent";
import { db } from "../../firebase";

import { styles } from "./MainScreen.style";

const MainScreen = (props) => {
  const email = props.route.params.email;
  const auth = getAuth();
  const [modalVisible, setModalVisible] = useState(false);
  const [picture, setPicture] = useState(null);
  const [about, setAbout] = useState("");
  const [text, setText] = useState("");
  const navigation = useNavigation();

  useEffect(async () => {
    const userDoc = await getDoc(doc(db, "users", email));
    userDoc.data()
      ? (setPicture(userDoc.data().pictures[0].url),
        setAbout(userDoc.data().about))
      : await setDoc(doc(db, "users", email), {
          about: "я молодец",
          likes: [],
          pictures: [],
          chats: [],
        });
  }, []);

  const handleSignOut = () => {
    auth
      .signOut()
      .then(() => navigation.navigate("Login"))
      .catch((error) => alert(error.message));
  };

  const updateInfo = async () => {
    await updateDoc(doc(db, "users", email), {
      about: text,
    });
    setAbout(text);
    setText("");
  };

  const pictureView = useMemo(() => {
    return picture ? (
      <View style={styles.pictureContainer}>
        <ImageComponent uri={picture} />
        <TextComponent text={about} style="about" />
      </View>
    ) : null;
  }, [picture]);

  return (
    <View style={styles.container}>
      {pictureView}
      <UploadModalComponent
        email={email}
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
      />
      <View style={styles.textInputContainer}>
        <TextInput
          style={styles.textInput}
          onChangeText={(text) => setText(text)}
          value={text}
          placeholder="text about"
        />
        <Icon
          name="address-book"
          type="font-awesome"
          color="#f50"
          onPress={updateInfo}
        />
      </View>
      <View style={styles.buttonContainer}>
        <ButtonComponent
          title="Upload"
          iconName="upload"
          buttonStyle="buttonStyle"
          containerStyle="containerStyle"
          onPress={() => setModalVisible(true)}
        />
        <ButtonComponent
          title="Sign Out"
          iconName="sign-out"
          buttonStyle="buttonStyle"
          containerStyle="containerStyle"
          onPress={handleSignOut}
        />
      </View>
    </View>
  );
};

export { MainScreen };
