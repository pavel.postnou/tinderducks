// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore/";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD0zPUTun4UWL93oSEfhlhp_xg9IFIrXSs",
  authDomain: "tinderducks-460ca.firebaseapp.com",
  projectId: "tinderducks-460ca",
  storageBucket: "tinderducks-460ca.appspot.com",
  messagingSenderId: "220616473177",
  appId: "1:220616473177:web:398bd96dd6c6298de18f48",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
export { app, db };
