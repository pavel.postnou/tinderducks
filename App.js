import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Icon } from "react-native-elements";

import { LoginScreen } from "./screens/LoginScreen";
import { MainScreen } from "./screens/MainScreen";
import { TinderScreen } from "./screens/TinderScreen";
import { PhotosScreen } from "./screens/PhotosScreen";
import { UsersScreen } from "./screens/UsersScreen";

export default function App() {
  const Stack = createNativeStackNavigator();
  const Tab = createBottomTabNavigator();
  function HomeTabs(props) {
    return (
      <Tab.Navigator
        screenOptions={{
          tabBarStyle: { height: 50, backgroundColor: "lightblue" },
        }}
      >
        <Tab.Screen
          options={{
            headerShown: false,
            tabBarIcon: () => (
              <Icon name="home" type="font-awesome" size={30} color="brown" />
            ),
          }}
          name="User"
          component={MainScreen}
          initialParams={{
            email: props.route.params.email,
          }}
        />
        <Tab.Screen
          options={{
            headerShown: false,
            tabBarIcon: () => (
              <Icon name="fire" type="font-awesome" size={30} color="orange" />
            ),
          }}
          name="Tinder"
          component={TinderScreen}
          initialParams={{
            email: props.route.params.email,
          }}
        />
        <Tab.Screen
          options={{
            headerShown: false,
            tabBarIcon: () => (
              <Icon name="users" type="font-awesome" size={30} color="orange" />
            ),
          }}
          name="Users"
          component={UsersScreen}
          initialParams={{
            email: props.route.params.email,
          }}
        />
        <Tab.Screen
          options={{
            headerShown: false,
            tabBarIcon: () => (
              <Icon
                name="picture-o"
                type="font-awesome"
                size={30}
                color="orange"
              />
            ),
          }}
          name="Photos"
          component={PhotosScreen}
          initialParams={{
            email: props.route.params.email,
          }}
        />
      </Tab.Navigator>
    );
  }
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          options={{ headerShown: false }}
          name="Login"
          component={LoginScreen}
        />
        <Stack.Screen
          options={{ headerShown: false }}
          name="Main"
          component={HomeTabs}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
