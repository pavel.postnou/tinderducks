import React from "react";
import { View, TouchableWithoutFeedback } from "react-native";

import ImageComponent from "../ImageComponent";
import TextComponent from "../TextComponent";

import { styles } from "./TouchableComponent.style";

const TouchableComponent = (props) => {
  const { onPress, item } = props;
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.container}>
        <ImageComponent uri={item.picture} />
        <TextComponent style="userName" text={item.email} />
      </View>
    </TouchableWithoutFeedback>
  );
};

export { TouchableComponent };
