import React, { useState } from "react";
import {
  View,
  Text,
  Modal,
  TouchableHighlight,
  ActivityIndicator,
  Image,
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { updateDoc, doc, arrayUnion } from "firebase/firestore/";

import { db } from "../../../../firebase";

import { styles } from "./UploadModelComponent.style";

export function UploadModalComponent(props) {
  const { email, modalVisible, setModalVisible } = props;
  const [image, setImage] = useState(null);
  const [uploading, setUploading] = useState(false);
  const storage = getStorage();

  const pickImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions,
      allowsEditing: true,
      aspect: [12, 16],
    });
    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  const uploadImage = async () => {
    if (image) {
      setUploading(true);
      const blob = await new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.onload = function () {
          resolve(xhr.response);
        };
        xhr.onerror = function (e) {
          console.log(e);
          reject(new TypeError("Network request failed"));
        };
        xhr.responseType = "blob";
        xhr.open("GET", image, true);
        xhr.send(null);
      });

      const refs = ref(storage, `${email}/${new Date().toISOString()}`);
      uploadBytes(refs, blob).then((snapshot) => {
        getDownloadURL(snapshot.ref).then(async (downloadURL) => {
          await updateDoc(doc(db, "users", email), {
            pictures: arrayUnion({
              url: downloadURL,
              fileName: snapshot.metadata.name,
            }),
          });
          setUploading(false);
          setModalVisible(false);
          setImage(null);
        });
      });
    } else alert("choose video");
  };

  return (
    <Modal animationType="fade" transparent={true} visible={modalVisible}>
      <View style={styles.centeredView}>
        {uploading ? (
          <View style={styles.activityIndicator}>
            <ActivityIndicator size={70} color="#0000ff" />
          </View>
        ) : (
          <View style={styles.modalView}>
            {image ? (
              <Image source={{ uri: image }} style={styles.video} />
            ) : null}
            <TouchableHighlight style={styles.modalUpload} onPress={pickImage}>
              <Text style={styles.text}>choose image</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.modalUpload}
              onPress={uploadImage}
            >
              <Text style={styles.text}>upload</Text>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.modalUpload}
              onPress={() => {
                setModalVisible(false), setImage(null);
              }}
            >
              <Text style={styles.text}>close</Text>
            </TouchableHighlight>
          </View>
        )}
      </View>
    </Modal>
  );
}
