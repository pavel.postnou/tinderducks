import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    video: {
      width: 350,
      height: 200,
    },
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 10,
    },
    modalView: {
      width: "90%",
      height: "70%",
      margin: 10,
      backgroundColor: "white",
      borderRadius: 10,
      padding: 10,
      alignItems: "stretch",
      shadowColor: "#000",
      justifyContent: "space-evenly",
    },
    modalUpload: {
      alignSelf: "center",
      backgroundColor: "#2196F3",
      padding: 5,
      borderRadius: 30,
      width: "95%",
      marginBottom: 10,
      alignItems: "center",
    },
    activityIndicator: {
      alignSelf:"center"
    }
  });
  