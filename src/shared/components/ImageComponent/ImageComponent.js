import React from "react";
import { Image } from "react-native";

import styles from "./ImageComponent.style";

const ImageComponent = (props) => {
  const uri = props.uri;
  return (
    <Image
      style={styles.image}
      source={{
        uri: uri,
      }}
      resizeMode="cover"
    />
  );
};

export { ImageComponent };
