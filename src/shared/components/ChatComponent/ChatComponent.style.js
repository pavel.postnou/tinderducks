import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  messageContainer: { width: "95%", margin: 10 },
  textContainerAuthor: {
    alignSelf: "flex-end",
  },
  textContainerFriend: {
    alignSelf: "flex-start",
  },
  myName: {
    fontWeight:"bold",
    color:"blue"
  },
  friendName: {
    fontWeight:"bold",
    color:"red"
  },
  text: {
    borderWidth:1,
    padding:4,
    backgroundColor:"white",
    borderRadius:10,
    fontWeight:"bold"
  }
});

export {styles};
