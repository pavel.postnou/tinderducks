import React from "react";
import { View, Text } from "react-native";

import styles from "./ChatComponent.style";

const ChatComponent = (props) => {
  const { email, item } = props;
  return (
    <View>
      {item.author === email ? (
        <View style={styles.messageContainer}>
          <View style={styles.textContainerAuthor}>
            <Text style={styles.myName}>{item.author}</Text>
          </View>
          <View style={styles.textContainerAuthor}>
            <Text style={styles.text}>{item.message}</Text>
          </View>
        </View>
      ) : (
        <View style={styles.messageContainer}>
          <View style={styles.textContainerFriend}>
            <Text style={styles.friendName}>{item.author}</Text>
          </View>
          <View style={styles.textContainerFriend}>
            <Text style={styles.text}>{item.message}</Text>
          </View>
        </View>
      )}
    </View>
  );
};

export { ChatComponent };
