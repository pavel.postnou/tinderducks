import React from "react";
import { Text } from "react-native";

import styles from "./TextComponent.style";

const TextComponent = (props) => {
  const { style, text } = props;

  return <Text style={styles[style]}>{text}</Text>;
};

export { TextComponent };
