import { StyleSheet } from "react-native";

import { width } from "../../../../constants/sizeConstants";

const styles = StyleSheet.create({
  about: {
    borderWidth: 1,
    borderColor: "grey",
    backgroundColor: "rgba(148, 148, 148, 0.7)",
    borderRadius: 10,
    padding: 2,
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
    position: "absolute",
    bottom: 10,
    width: "90%",
  },
  tinderAbout: {
    borderWidth: 1,
    borderColor: "grey",
    backgroundColor: "rgba(148, 148, 148, 0.7)",
    borderRadius: 10,
    padding: 2,
    color: "white",
    fontSize: 15,
    fontWeight: "bold",
    width: width - 70,
  },
  like: {
    borderWidth: 1,
    borderColor: "lightgreen",
    color: "lightgreen",
    fontSize: 32,
    fontWeight: "bold",
    padding: 10,
  },
  nope: {
    borderWidth: 1,
    borderColor: "red",
    color: "red",
    fontSize: 32,
    fontWeight: "bold",
    padding: 10,
  },
  userName: {
    fontWeight: "bold",
  },
});

export default styles;
