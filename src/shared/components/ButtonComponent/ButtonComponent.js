import React from "react";
import { Button } from "react-native-elements";

import styles from "./ButtonComponent.style";

const ButtonComponent = (props) => {
  const { title, iconName, onPress, buttonStyle, containerStyle } = props;

  return (
    <Button
      title={title}
      titleStyle={styles.titleStyle}
      buttonStyle={styles[buttonStyle]}
      containerStyle={styles[containerStyle]}
      icon={{
        name: iconName,
        type: "font-awesome",
        size: 20,
        color: "white",
      }}
      iconRight
      iconContainerStyle={styles.iconContainerStyle}
      onPress={onPress}
    />
  );
};

export { ButtonComponent };
