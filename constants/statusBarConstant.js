import { StatusBar } from "react-native";

const statusBarHeight = StatusBar.currentHeight;

export default statusBarHeight;
